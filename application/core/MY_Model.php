<?php
defined('BASEPATH') OR exit('No direct script access allowed');
  class MY_Model extends CI_Model {
    // variables
    protected $_table_name     = '';
    /**
    * This model's default primary key or unique identifier.
    * Used by the get(), update() and delete() functions.
    */
    protected $_primary_key    = 'id';
    protected $_primary_filter = ''; //intval
    protected $_order_by       = '';
    protected $_rules          = array();
    protected $_timestamps     = FALSE;

  function __construct() {
    /* Call the Model constructor */
    parent::__construct();
  }
  //---------------------------------------------------------------------------
  //  C R U D methods
  //---------------------------------------------------------------------------
  function get($id = NULL , $single = FALSE){
    if($id != NULL){
      $filter = $this->_primary_filter;
      $id     = $filter($id);
      $this->db->where($this->_primary_key, $id);
      $method = 'row';
    }elseif($single = TRUE){
      $method = 'result';
    }
    if(!count($this->db->ar_order_by)){
      $this->db->order_by($this->_order_by);
    }
  }

  function get_by($where, $single = FALSE){
    $this->db->where($where);
    return $this->db->get(NULL, $single);
  }

  function save($data , $id = NULL){
    // SET timestamps
    if($this->_timestamps == TRUE){
      $now                    = ('Y-m-d H:i:s');
      $id || $data['created'] = $now;
      $data['modified']       = $now;
    }
    // INSERT
    if($id === NULL){
      !isset($data[$this->_primary_key]) || $data[$this->_primary_key] == NULL;
      $this->db->insert($data);
      $id = $this->db->insert_id();
    }else{
    // UPDATE
      $filter = $this->_primary_filter;
      $id     = $filter($data[$this->_primary_key]);
      $this->db->set($data);
      $this->db->where($this->_primary_key, $id);
      $this->db->update($this->_table_name);
    }
    return $id;
  }
  function delete($id , $limit = 1){
    $filter = $this->_primary_filter;
    $id     = $filter($id);
    if(!$id){
      return FALSE;
    }
    $this->db->where($this->_primary_key, $id);
    $this->db->limit($limit);
    $this->db->delete();
  }
}
?>
