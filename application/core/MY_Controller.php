<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends MX_Controller {

	public $data = array();

	public function __construct(){
		parent::__construct();
		$this->data['site_name'] = config_item('site_name');
		$this->load->module('template/template');
		$this->load->module('users/auth');
	}

}

?>
