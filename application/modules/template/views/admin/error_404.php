<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title><?php print $regs_status->title; ?>  | 404 Page not found</title>

    <!-- Bootstrap 3.3.6 -->
    <link rel="stylesheet" href="<?php print  base_url();?>assets/bootstrap-3.3.7-dist/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?php print  base_url();?>assets/font-awesome/css/font-awesome.min.css">

    <style type="text/css">
    body {
    	background-color: #fff;
    	margin: 40px;
    	font: 13px/20px normal Helvetica, Arial, sans-serif;
    	color: #4F5155;
    }

    a {
    	color: #003399;
    	background-color: transparent;
    	font-weight: normal;
    }

    h1 {
    	color: #444;
    	background-color: transparent;
    	border-bottom: 1px solid #D0D0D0;
    	font-size: 19px;
    	font-weight: normal;
    	margin: 0 0 14px 0;
    	padding: 14px 15px 10px 15px;
    }

    code {
    	font-family: Consolas, Monaco, Courier New, Courier, monospace;
    	font-size: 12px;
    	background-color: #f9f9f9;
    	border: 1px solid #D0D0D0;
    	color: #002166;
    	display: block;
    	margin: 14px 0 14px 0;
    	padding: 12px 10px 12px 10px;
    }

    #container {
    	margin: 10px;
    	border: 1px solid #D0D0D0;
    	box-shadow: 0 0 8px #D0D0D0;
    }

    p {
    	margin: 12px 15px 12px 15px;
    }
    </style>

</head>

<body class="gray-bg">

    <div class="container">

            <div class="middle-box text-center animated fadeInDown">
                <h1>404 Page Not Found</h1>
                <h3 class="font-bold"></h3>

                <div class="error-desc">
                    <p>We could not find the page you were looking for. Meanwhile, you may return to <a href="<?php print base_url('poll');?>">Dashboard</a></p>
                </div>

                </p><strong>Copyright &copy; 2018 <a href="#"><?php print $regs_status->sitename; ?></a>.</strong> All rights reserved. </p>
                <div class="">
                    <b>Version</b> 1.0
                </div>
            </div>

    </div>

    <!-- Mainly scripts -->
    <!-- <script src="js/jquery-3.1.1.min.js"></script> -->
    <!-- jQuery 2.2.3 -->
    <script src="<?php print base_url(); ?>assets/plugins/jQuery/jquery-2.2.3.min.js"></script>
    <!-- Bootstrap 3.3.6 -->
    <script src="<?php print base_url(); ?>assets/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>

</body>

</html>
