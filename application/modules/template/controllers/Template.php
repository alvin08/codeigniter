<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Template extends MY_Controller{

  public function __construct(){
    parent::__construct();
  }

  public function admin($file_path, $data = NULL){
    $this->load->view('template/admin/header', $data); // header.php
    $this->load->view('template/admin/main-header', $data); //main-header.php
    $this->load->view('template/admin/main-sidebar', $data); //main-sidebar.php
    $this->load->view($file_path , $data); //content-wrapper
    $this->load->view('template/admin/main-footer', $data); // main-footer.php
    $this->load->view('template/admin/footer', $data); // footer.php

  }

  public function load_static_template($file_path, $data= NULL){
    $this->load->view('template/static/header');
    $this->load->view($file_path, $data);
    $this->load->view('template/static/footer');
  }
}
