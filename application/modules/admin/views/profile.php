  <!-- =============================================== -->
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->

    <!-- Main content -->
    <section class="content">
      <!-- Default box -->
      <div class="col-sm-3">
        <div class="box box-primary">

          <div class="box-body box-profile">
              <img class="profile-user-img img-responsive img-circle" src="<?php print base_url();?>assets/AdminLTE-2.4.3/dist/img/user4-128x128.jpg" alt="User profile picture">

              <h3 class="profile-username text-center"><?php print $user->username; ?></h3>

              <p class="text-muted text-center">Role: <?php print ucfirst($groups->name); ?></p>

              <!-- <ul class="list-group list-group-unbordered">
                <li class="list-group-item">
                  <b>Followers</b> <a class="pull-right">1,322</a>
                </li>
                <li class="list-group-item">
                  <b>Following</b> <a class="pull-right">543</a>
                </li>
                <li class="list-group-item">
                  <b>Friends</b> <a class="pull-right">13,287</a>
                </li>
              </ul>

              <a href="#" class="btn btn-primary btn-block"><b>Follow</b></a> -->
            </div>
        </div>
        <div class="panel panel-default">
          <ul class="nav">
            <li class="active"><a href="#about" data-toggle="tab"><i class="fa fa-user">&nbsp;</i>About</a></li>
            <li><a href="#change_pic" data-toggle="tab"><i class="fa fa-exchange">&nbsp;</i>Change Avatar</a></li>
            <li><a href="#change_pass" data-toggle="tab"><i class="fa fa-lock">&nbsp;</i>Change Password</a></li>
            <li><a href="#tab-edit" data-toggle="tab"><i class="fa fa-pencil">&nbsp;</i>Edit</a></li>
          </ul>
        </div>
      </div>
      <div class="col-sm-9">
        <div class="box box-primary">
            <div id="infoMessage"><?php echo $message;?></div>
            <div class="nav-tabs-custom">

              <div class="tab-content">
                <div class="active tab-pane" id="about">
                  <div class="panel panel-default">
                    <div class="panel-heading">
                      <h4>About</h4>
                    </div>
                    <div class="panel-body">
                      <div class="about-area">
                        <h4>Personal Information</h4>
                        <div class="table-responsive">
                          <table class="table about-table">
                            <tbody>
                              <tr>
                                <th>Full Name</th>
                                <td><?php print '<span>'.ucfirst($user->last_name).'</span><span>'.$user->first_name.'</span>'; ?></td>
                              </tr>
                              <tr>
                                <th>Country</th>
                                <td></td>
                              </tr>
                              <tr>
                                <th>Last Login</th>
                                <td><?php print date('jS F Y h:i:s A (T)',$user->last_login);?></td>
                              </tr>
                              <tr>
                                <th>Email</th>
                                <td><?php print $user->email;?></td>
                              </tr>
                              <tr>
                                <th>Phone</th>
                                <td><?php print $user->phone;?></td>
                              </tr>
                              <tr>
                                <th>Created Date</th>
                                <td><?php print $user->created_on;?></td>
                              </tr>
                              <tr>
                                <th>Mobile No</th>
                                <td></td>
                              </tr>
                            </tbody>
                          </table>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- /.tab-pane -->
                <div class="tab-pane" id="change_pic">
                  <div class="panel" style="visibility: visible; opacity: 1; display: block; transform: translateY(0px);">
                    <div class="panel-heading">
                      <h2>Change Profile Picture</h2>
                    </div>
                    <div class="panel-body">
                      <div class="row">
                        <div class="col-md-12">
                          <form class="form-horizontal" enctype="multipart/form-data" method="post" action="" role="form">
                            <div class="form-group">
                              <div class="col-lg-6">
                                <img src="http://testingcodes.com/login/public/assets/img/default_user.png" class="img-responsive img-circle" alt="" width="200">
                                <br><br>
                                <input name="chang_image" type="file">
                              </div>
                            </div>
                            <div class="form-group">
                              <div class="col-lg-offset-2 col-lg-10">
                                <button type="submit" class="btn btn-success">Change</button>
                                <button type="button" class="btn btn-default">Cancel</button>
                              </div>
                            </div>
                          </form>

                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- /.tab-pane -->

                <div class="tab-pane" id="change_pass">
                  <div class="panel panel-default">
                    <div class="panel panel-heading">
                      <h4>Change Password</h4>
                    </div>
                    <div class="panel-body">
                      <?php echo form_open(base_url('admin/dashboard/profile/'.$user->id));?>
                      <div class="form-group row">
                        <div class="col-sm-3">
                          <?php echo lang('edit_user_password_label', 'password');?> <br />
                        </div>
                        <div class="col-sm-9">
                          <?php echo form_input($password);?>
                          <?php print form_error('password'); ?>
                        </div>
                      </div>

                      <div class="form-group row">
                        <div class="col-sm-3">
                          <?php echo lang('edit_user_password_confirm_label', 'password_confirm');?><br />
                        </div>
                        <div class="col-sm-9">
                          <?php echo form_input($password_confirm);?>
                          <?php print form_error('password_confirm'); ?>
                        </div>
                      </div>
                      <?php echo form_hidden('id', $user->id);?>
                      <?php echo form_hidden($csrf); ?>
                      <?php echo form_submit('change_password', 'Change Password', array('class' => 'pull-right btn btn-primary'));?>
                      <?php echo form_close();?>

                    </div>
                  </div>
                </div>
                <!-- /.tab-pane -->
                <div class="tab-pane" id="tab-edit">
                  <div class="panel panel-default">
                    <div class="panel-heading">
                      <h4>Edit</h4>
                    </div>
                    <div class="panel-body">
                      <div class="row">
                        <div class="col-md-12">
                          <?php echo form_open(base_url('admin/dashboard/profile/'.$user->id));?>
                          <div class="form-group row">
                            <div class="col-sm-3">
                              <?php echo lang('edit_user_fname_label', 'first_name');?>
                            </div>
                            <div class="col-sm-9">
                              <?php echo form_input($first_name);?>
                            </div>
                          </div>

                          <div class="form-group row">
                            <div class="col-sm-3">
                              <?php echo lang('edit_user_lname_label', 'last_name');?> <br />
                            </div>
                            <div class="col-sm-9">
                              <?php echo form_input($last_name);?>
                            </div>
                          </div>

                          <div class="form-group row">
                            <div class="col-sm-3">
                              <?php echo lang('edit_user_company_label', 'company');?> <br />
                            </div>
                            <div class="col-sm-9">
                              <?php echo form_input($company);?>
                            </div>
                          </div>

                          <div class="form-group row">
                            <div class="col-sm-3">
                              <?php echo lang('edit_user_phone_label', 'phone');?> <br />
                            </div>
                            <div class="col-sm-9">
                              <?php echo form_input($phone);?>
                            </div>
                          </div>
                          <?php echo form_hidden('id', $user->id);?>
                          <?php echo form_hidden($csrf); ?>
                          <?php echo form_submit('update_profile', 'Update' , array('class' => 'pull-right btn btn-primary'));?>
                          <?php echo form_close();?>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- /.tab-pane -->
              </div>
              <!-- /.tab-content -->
            </div>
        </div>
        <!-- /.nav-tabs-custom -->
      </div>
      <!-- /.box -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

    <!-- =============================================== -->
