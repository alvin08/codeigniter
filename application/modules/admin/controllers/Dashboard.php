<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MY_Controller{

  public function __construct(){
    parent::__construct();
    //Codeigniter : Write Less Do More
    if (!$this->ion_auth->logged_in()){
      redirect('users/auth/login', 'refresh');
    }
    $this->load->model('users/ion_auth_model');
    $this->data['userid'] = $this->session->userdata('user_id');
  }

  function index(){
    $this->template->admin('admin/dashboard');
  }

  function profile($id){
    $this->session->set_userdata('referred_from', current_url());
    $referred_from = $this->session->userdata('referred_from');
    $user = $this->ion_auth->user($id)->row();
    $groups = $this->ion_auth->groups()->row();

    // validate form input
    if($this->input->post('update_profile')){
      $this->form_validation->set_rules('first_name', $this->auth->lang->line('edit_user_validation_fname_label'), 'trim|required');
      $this->form_validation->set_rules('last_name', $this->auth->lang->line('edit_user_validation_lname_label'), 'trim|required');
      $this->form_validation->set_rules('phone', $this->auth->lang->line('edit_user_validation_phone_label'), 'trim|required');
      $this->form_validation->set_rules('company', $this->auth->lang->line('edit_user_validation_company_label'), 'trim|required');
    }

    if (isset($_POST) && !empty($_POST)){
      // do we have a valid request?
      if ($this->auth->_valid_csrf_nonce() === FALSE || $id != $this->input->post('id')){
        show_error($this->auth->lang->line('error_csrf'));
      }

      // update the password if it was posted
      if ($this->input->post('password')){
        $this->form_validation->set_rules('password', $this->auth->lang->line('edit_user_validation_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[password_confirm]');
        $this->form_validation->set_rules('password_confirm', $this->auth->lang->line('edit_user_validation_password_confirm_label'), 'required');
      }

      if ($this->form_validation->run() === TRUE){
        $data = array(
          'first_name' => $this->input->post('first_name'),
          'last_name' => $this->input->post('last_name'),
          'company' => $this->input->post('company'),
          'phone' => $this->input->post('phone'),
        );

        // update the password if it was posted
        if ($this->input->post('password')){
          $data['password'] = $this->input->post('password');
        }

        // check to see if we are updating the user
        if ($this->ion_auth->update($user->id, $data)){
          // redirect them back to the admin page if admin, or to the base url if non admin
          $this->session->set_flashdata('message', $this->ion_auth->messages());
          redirect($referred_from, 'refresh');

        }else{
          // redirect them back to the admin page if admin, or to the base url if non admin
          $this->session->set_flashdata('message', $this->ion_auth->errors());
          redirect($referred_from, 'refresh');

        }

      }
    }

    // display the edit user form
    $this->data['csrf'] = $this->auth->_get_csrf_nonce();

    // set the flash data error message if there is one
    $this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));

    // pass the user to the view
    $this->data['user'] = $user;
    $this->data['groups'] = $groups;

    $this->data['first_name'] = array(
      'name'  => 'first_name',
      'id'    => 'first_name',
      'type'  => 'text',
      'value' => $this->form_validation->set_value('first_name', $user->first_name),
      'class'=> 'form-control',
    );
    $this->data['last_name'] = array(
      'name'  => 'last_name',
      'id'    => 'last_name',
      'type'  => 'text',
      'value' => $this->form_validation->set_value('last_name', $user->last_name),
      'class'=> 'form-control',
    );
    $this->data['company'] = array(
      'name'  => 'company',
      'id'    => 'company',
      'type'  => 'text',
      'value' => $this->form_validation->set_value('company', $user->company),
      'class'=> 'form-control',
    );
    $this->data['phone'] = array(
      'name'  => 'phone',
      'id'    => 'phone',
      'type'  => 'text',
      'value' => $this->form_validation->set_value('phone', $user->phone),
      'class'=> 'form-control',
    );
    $this->data['password'] = array(
      'name' => 'password',
      'id'   => 'password',
      'type' => 'password',
      'class'=> 'form-control',
    );
    $this->data['password_confirm'] = array(
      'name' => 'password_confirm',
      'id'   => 'password_confirm',
      'type' => 'password',
      'class'=> 'form-control',
    );

    $this->template->admin('admin/profile', $this->data);

  }

}
