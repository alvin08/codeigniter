<div class="login-box">
  <div class="login-logo">
    <a href="<?php print base_url();?>assets/AdminLTE-2.4.3/index2.html"></a>
    <?php echo lang('login_heading');?>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
    <p class="login-box-msg"><?php echo lang('reset_password_heading');?></p>
		<?php echo form_open('users/auth/reset_password/' . $code);?>

			<div class="form-group">
				<label for="new_password"><?php echo sprintf(lang('reset_password_new_password_label'), $min_password_length);?></label>
				<?php echo form_input($new_password);?>
				<?php print form_error('new'); ?>
			</div>

			<div class="form-group">
				<label for="new_password_confirm"><?php echo lang('reset_password_new_password_confirm_label', 'new_password_confirm');?></label>
				<?php echo form_input($new_password_confirm);?>
				<?php print form_error('new_confirm'); ?>

			</div>

			<?php echo form_input($user_id);?>
			<?php echo form_hidden($csrf); ?>

			<?php echo form_submit('submit', lang('reset_password_submit_btn'), array('class' => 'btn btn-primary btn-block'));?>

		<?php echo form_close();?>

  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->
