


<!-- =============================================== -->

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->

  <!-- Main content -->
  <section class="content">

    <!-- Default box -->
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title"><?php echo lang('edit_user_heading');?></h3>
      </div>
      <div class="box-body">
        <h1></h1>
        <div id="infoMessage"><?php echo $message;?></div>

        <?php echo form_open(uri_string());?>

              <div class="form-group row">
                <div class="col-sm-3">
                  <?php echo lang('edit_user_fname_label', 'first_name');?>
                </div>
                <div class="col-sm-9">
                  <?php echo form_input($first_name);?>
                </div>
              </div>

              <div class="form-group row">
                <div class="col-sm-3">
                  <?php echo lang('edit_user_lname_label', 'last_name');?> <br />
                </div>
                <div class="col-sm-9">
                  <?php echo form_input($last_name);?>
                </div>
              </div>

              <div class="form-group row">
                <div class="col-sm-3">
                  <?php echo lang('edit_user_company_label', 'company');?> <br />
                </div>
                <div class="col-sm-9">
                  <?php echo form_input($company);?>
                </div>
              </div>

              <div class="form-group row">
                <div class="col-sm-3">
                  <?php echo lang('edit_user_phone_label', 'phone');?> <br />
                </div>
                <div class="col-sm-9">
                  <?php echo form_input($phone);?>
                </div>
              </div>

              <div class="form-group row">
                <div class="col-sm-3">
                  <?php echo lang('edit_user_password_label', 'password');?> <br />
                </div>
                <div class="col-sm-9">
                    <?php echo form_input($password);?>
                  </div>
              </div>

              <div class="form-group row">
                <div class="col-sm-3">
                  <?php echo lang('edit_user_password_confirm_label', 'password_confirm');?><br />
                </div>
                <div class="col-sm-9">
                  <?php echo form_input($password_confirm);?>
                </div>
              </div>
              <div class="checkbox">

              <?php if ($this->ion_auth->is_admin()): ?>
                <div class="col-sm-3">
                  <h4><?php echo lang('edit_user_groups_heading');?></h4>
                </div>
                <div class="col-sm-3">
                  <?php foreach ($groups as $group):?>
                      <label class="checkbox">
                        <?php
                        $gID=$group['id'];
                        $checked = null;
                        $item = null;
                        foreach($currentGroups as $grp) {
                          if ($gID == $grp->id) {
                            $checked= ' checked="checked"';
                            break;
                          }
                        }
                        ?>
                        <input type="checkbox" name="groups[]" value="<?php echo $group['id'];?>"<?php echo $checked;?>>
                        <?php echo htmlspecialchars($group['name'],ENT_QUOTES,'UTF-8');?>
                      </label>
                  <?php endforeach?>
                </div>

              <?php endif ?>

              <?php echo form_hidden('id', $user->id);?>
              <?php echo form_hidden($csrf); ?>


            </div>

      </div>
      <!-- /.box-body -->
      <div class="box-footer">
              <div class="form-group row"><?php echo form_submit('submit', lang('edit_user_submit_btn'));?></div>
      </div>
      <?php echo form_close();?>

      <!-- /.box-footer-->
    </div>
    <!-- /.box -->

  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->

  <!-- =============================================== -->
