<!-- =============================================== -->

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->

  <!-- Main content -->
  <section class="content">

<div class="col-sm-6">
  <!-- Default box -->
  <div class="box box-primary">
    <div class="box-header with-border">
      <h3 class="box-title"><?php echo lang('deactivate_heading');?></h3>
    </div>
    <div class="box-body">
      <p><?php echo sprintf(lang('deactivate_subheading'), $user->username);?></p>

      <?php echo form_open("users/auth/deactivate/".$user->id);?>

        <p>
          <?php echo lang('deactivate_confirm_y_label', 'confirm');?>
          <input type="radio" name="confirm" value="yes" checked="checked" />
          <?php echo lang('deactivate_confirm_n_label', 'confirm');?>
          <input type="radio" name="confirm" value="no" />
        </p>

        <?php echo form_hidden($csrf); ?>
        <?php echo form_hidden(array('id'=>$user->id)); ?>


    </div>
    <!-- /.box-body -->
    <div class="box-footer">
      <?php echo form_submit('submit', lang('deactivate_submit_btn'), array('class' => 'btn btn-primary btn-block btn-flat', 'type' => 'button'));?>
    <?php echo form_close();?>
    </div>
    <!-- /.box-footer-->
  </div>
  <!-- /.box -->
</div>

  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->

  <!-- =============================================== -->
