<!-- =============================================== -->
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->

  <!-- Main content -->
  <section class="content">
      <!-- Default box -->
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title"><?php echo lang('create_user_heading');?></h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">
          <?php echo form_open('users/auth/create_user');?>
              <div class="box-body">
                <div class="form-group row">
                  <div class="col-sm-3">
                    <label for="email"><?php echo lang('create_user_email_label', 'email');?></label>
                    <?php
                    if($identity_column !=='email'){
                      print lang('create_user_identity_label', 'identity');
                    }
                    ?>
                  </div>
                  <div class="col-sm-9">
                    <?php echo form_input($email);?>
                    <?php echo form_error('email'); ?>
                    <?php
                    if($identity_column!=='email') {
                      echo form_error('identity');
                      echo form_input($identity);
                    }
                    ?>
                  </div>
                </div>
                <div class="form-group row">
                  <div class="col-sm-3">
                    <label for="first_name"><?php echo lang('create_user_fname_label', 'first_name');?></label>
                  </div>
                  <div class="col-sm-9">
                    <?php echo form_input($first_name);?>
                    <?php echo form_error('first_name'); ?>
                  </div>
                </div>

                <div class="form-group row">
                  <div class="col-sm-3">
                    <label for="first_name"><?php echo lang('create_user_lname_label', 'last_name');?></label>
                  </div>
                  <div class="col-sm-9">
                    <?php echo form_input($last_name);?>
                    <?php echo form_error('last_name'); ?>
                  </div>
                </div>

                <div class="form-group row">
                  <div class="col-sm-3">
                    <label for="phone"><?php echo lang('create_user_phone_label', 'phone');?></label>
                  </div>
                  <div class="col-sm-9">
                    <?php echo form_input($phone);?>
                    <?php echo form_error('phone'); ?>
                  </div>
                </div>

                <div class="form-group row">
                  <div class="col-sm-3">
                    <label for="phone"><?php echo lang('create_user_company_label', 'company');?></label>
                  </div>
                  <div class="col-sm-9">
                    <?php echo form_input($company);?>
                    <?php echo form_error('company'); ?>
                  </div>
                </div>
                <div class="form-group row">
                  <div class="col-sm-3">
                    <label for="password"><?php echo lang('create_user_password_label', 'password');?></label>
                  </div>
                  <div class="col-sm-9">
                    <?php echo form_input($password);?>
                    <?php echo form_error('password'); ?>
                  </div>
                </div>
                <div class="form-group row">
                  <div class="col-sm-3">
                    <label for="password_confirm"><?php echo lang('create_user_password_confirm_label', 'password_confirm');?></label>
                  </div>
                  <div class="col-sm-9">
                    <?php echo form_input($password_confirm);?>
                    <?php echo form_error('password_confirm'); ?>
                  </div>
                </div>
                <div class="form-group row">
                  <div class="col-sm-3">
                    <label for="status">Status:</label>
                  </div>
                  <div class="col-sm-9">
                    <label for="status">Active<?php print form_radio(array("name"=>"status","value"=> 1)); ?></label>
                    <label for="status">Inactive<?php print form_radio(array("name"=>"status","value"=> 0)); ?></label>
                  </div>
                </div>
              </div>
              <div class="box-footer pull-right">
              <button type="reset" class="btn btn-primary btn-flat">Reset</button>
            <?php echo form_submit('submit', lang('create_user_submit_btn'), array('class' => 'btn btn-success btn-flat', 'type' => 'button'));?>
              </div>
              <?php echo form_close();?>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->

  <!-- =============================================== -->
