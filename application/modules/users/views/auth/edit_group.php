<!-- =============================================== -->

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->

  <!-- Main content -->
  <section class="content">

<div class="col-sm-6">
  <!-- Default box -->
  <div class="box box-primary">
    <div class="box-header with-border">
      <h3 class="box-title"><?php echo lang('edit_group_heading');?></h3>
      <div class="box-tools pull-right">
        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
      </div>
    </div>
    <div class="box-body">
      <div id="infoMessage"><?php echo $message;?></div>

      <?php echo form_open(current_url());?>

            <p>
                  <?php echo lang('edit_group_name_label', 'group_name');?> <br />
                  <?php echo form_input($group_name);?>
            </p>

            <p>
                  <?php echo lang('edit_group_desc_label', 'description');?> <br />
                  <?php echo form_input($group_description);?>
            </p>


    </div>
    <!-- /.box-body -->
    <div class="box-footer">
      <p><?php echo form_submit('submit', lang('edit_group_submit_btn'), array('class' => 'btn btn-primary btn-block btn-flat', 'type' => 'button'));?></p>
      <?php echo form_close();?>
    </div>
    <!-- /.box-footer-->
  </div>
  <!-- /.box -->
</div>

  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->

  <!-- =============================================== -->
