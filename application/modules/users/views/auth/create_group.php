<!-- =============================================== -->

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->

  <!-- Main content -->
  <section class="content">

    <!-- Default box -->
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title"><?php echo lang('create_group_heading');?></h3>
        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                  title="Collapse">
            <i class="fa fa-minus"></i></button>
          <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
            <i class="fa fa-times"></i></button>
        </div>
      </div>
      <div class="box-body">
        <h1></h1>
        <p><?php echo lang('create_group_subheading');?></p>

        <div id="infoMessage"><?php echo $message;?></div>

        <?php echo form_open("users/auth/create_group");?>

              <p>
                    <?php echo lang('create_group_name_label', 'group_name');?> <br />
                    <?php echo form_input($group_name);?>
              </p>

              <p>
                    <?php echo lang('create_group_desc_label', 'description');?> <br />
                    <?php echo form_input($description);?>
              </p>


      <!-- /.box-body -->
      <div class="box-footer">
        <?php echo form_submit('submit', lang('create_group_submit_btn'), array('class' => 'btn btn-primary btn-block btn-flat', 'type' => 'button'));?>

  <?php echo form_close();?>      </div>
      </div>
      <!-- /.box-footer-->
    </div>
    <!-- /.box -->

  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->

  <!-- =============================================== -->
